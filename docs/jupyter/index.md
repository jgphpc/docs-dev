# Experimental Jupyter Features

Since 2015 [Jupyter](https://docs.nersc.gov/services/jupyter/) has been supported at NERSC in some form or fashion.
Along the way we have expanded resources available to Jupyter users and added new features to improve their experience.
Some of the work we do involves customizing our Jupyter and JupyterHub deployment and contributing back to the broader community.

We like to experiment with new features and have users test them out and give us feedback.
We have created this page to provide supplemental documentation of those experimental features.
If the experiments are successful we can proceed with integrating them into our production deployment.
If they don't work out, well, at least we learned something!

Below you will find a list of currently experimental features that are available or under active development.
For each case, there is a description of the problem it solves and how you can opt in/out of testing it.
In general, if you have feedback, opening a [ticket](https://help.nersc.gov) about it is just fine.
A ticket is also the best way to get help if problems arise.

If you use these features, please be sure to check this page and the update history frequently.
And finally, remember these are **EXPERIMENTAL** features:

* Do not make them the mission-critical centerpiece of your project if they are listed here.
* Expect them to develop over time, hopefully getting better, but there will be changes.
* Please share your impressions and feedback.
* If *you* learn something that would be useful to document here, please make a pull request against the [docs!](https://gitlab.com/NERSC/docs-dev)

## Profile Manager

JupyterLab at NERSC is managed centrally.
We install the stack that you use when you start up JupyterLab on Cori "bare metal."
For most users this is fine, but for others it is less than ideal.

One issue is that JupyterLab extensions are installed in a bundle.
This means that users have no way to install their own extensions and have them work with the central installation.
This is a problem for people who need a special extension that won't be broadly useful to other users.
It's also a problem if you're a user trying to actively develop your own JupyterLab extension and test it.
There are signs that JupyterLab 3 may address this issue, and we will evaluate it when it becomes ready.

Since we develop extensions, we needed a solution for this ourselves.
We're working to generalize our solution and make it easier for users to use.
The idea is that users will pick from Shifter images or conda environments that can run JupyterLab.
These have to meet some requirements before they can be used (see below).
Picking those "profiles" will be done on the Hub, or through a service associated with the Hub.
This will allow users to choose a "default" profile other than the NERSC-provided one.

At this point in time (December 2020) we provide support for Shifter image-based profiles.
We'll be evaluating the best way to provide conda environment-based profiles in Q1 2021.

### Trying it Out

On the main JupyterHub home page you will see a "Services" dropdown menu in the nav bar.
Open it and click "profile-manager."

The UI will populate a dropdown box list of available Shifter images you can use.
Pick one and click the "change" button next to it.
Under "Your selected Shifter image" above it should change to the name of the image you want to run.
This is now your default profile.

Click "Home" to go back to the JupyterHub home page and start up a Cori shared CPU node session.
Open a notebook and then do something like

    import sys
    sys.executable

You should see that the path to the python executable isn't what you'd normally see on Cori.

If you want to go back to NERSC's JupyterLab, go back to the profile-manager UI from the JupyterHub home page.
There's a big, big orange button to clear your selection.

### Requirements

Not just any Shifter image can be used with this service.
To get an image to show up here, and work properly, you need to build a Docker image:

* Include the packages `jupyterlab` and `jupyterhub` at the very least
* Include `ENV NERSC_JUPYTER_IMAGE=YES` in the Dockerfile (exactly like that)
* Use `ENV PATH=<path/to/python>:$PATH` to set the path to python and `jupyterhub-singleuser`
* Push the image to a registry
* Pull the image onto Cori and covert it to Shifter with `shifterimg pull <imagename>`

How you set up the image is up to you.
We'll create a barebones image that you could use that takes care of the first 3 of the above points.
But you could start from scratch.

Once it's pulled to Cori, and if it includes the environment variable setting mentioned above, it should show up in the UI.
There is a little bit of caching in the UI so it might take a minute to refresh.

### Known Issues (as of 2020-12-11)

* Shifter-based images cannot run Shifter-based kernels.
  This may not be that big a deal since it makes sense to put everything you need into the Shifter image.

* Shifter-based images cannot run Slurm commands.
  This may be a show-stopper for a lot of people.
  It isn't going to be a show-stopper for everyone.

* The terminal environment is tricky.
  Basically it doesn't pick up Cori's `/etc/profiles` directory.
  Work in progress.

* There's an external dependency on the Shifter image gateway running on Cori.
  If this goes down then you may not be able to set a new Shifter image and won't see the list.
  Clearing your Shifter image selection should still work in that case.
  If it looks like you can't select images, please alert us via ticket.
